

Welcome to simple_karma!

This is not a "simple" module. The name is misleading.
It was meant to be simple. But then, new ideas and features kept on coming
in my head, and the module become more and more powerful...

Here is what the module can do:

 - Javascript voting; however, the module works 1000% even if the
   user doesn't have Javascript

 - Abuse control. The module allows you to limit users on how many karma points
   they can give, to a particular user or in general. The admin can also decide
   on how many different comments/nodes a user can vote on

 - Publishing the best comment in a special spot, ONLY for nodes associated
   to selected (configurable) terms. This allows certain forums (for
   example) to have this feature, and create a "Ask Jeeves"-like
   web sites

 - Automatic assignment of roles depending on the karma range. This means
   that you can make some users more powerful according to the
   karma they have. It's DEADLY CRUCIAL that you create roles SPECIFICALLY
   for this module. This module will turn the set roles on and off, with
   no mercy. Roles are assigned when a user signs up, which means that
   you can assign a "default role" to a user.

 - Automatic assignment of userpoints based on a custom formula when
   karma is assigned. The Karma module will take/give userpoints according
   to the karma received/taken away. This is handy if you allow people to
   "spend" their user points, for example.
   
 - Bonus karma-voting owers to people with specific permissions. The module
   sets a number of specific permissions. When a user has them (because
   they are associated to a role, for example), then the user can give/take
   more karma on each vote.

 - Ability to bury comments below a certain karma. The administrator
   can set a threshold under which a comment is marked as buried. The
   comment will still show up, but users won't be able to vote for it
   nor see its contents.

 - Ability for the karma administrator to see all of the buried
   comments and vote on them (users can't vote for buried comments). This
   way, the administrator could un-bury them, or use this as
   anti-abuse mechanism.

 - No HTML anywhere in the module. Everything is configurable from the
   admin interface

 - Ability to put the karma form in the link section of the comment/node
   or anywhere in the comment template (by hand)

 - Ability to see users by karma, and (for the karma administrator) the
 - ability to see who got karma when

 - Karma points expire in N days (where N is configurable). NOTE:
   points expire only as far as users are concerned. All of the
   queries consider ALL of the karma votes for comments. This makes sense:
   a user's karma will tend to 0 as time passes, whereas a comment's
   karma will stay forever

* ...and much more. Have a look at the configuration options.

Enjoy!

